Las capas de la aplicacion (por ejemplo capa de persistencia, vistas, red, negocio, etc) y que clases perte-
necen a cual:
tiene 5 capas
-vista: homeview,listview,detalleview
-interactor:homeInteractor,ListInteractor,DetalleInteractor
-presenter:homePresenter,listPresenter,detallePresenter
-entity:movieModel
-router:homeWireFrame, ListWireFrame, detalleWireFrame

principio de responsabilidad unica?
en que se busca que cada clase y/o metodo se encargue de realizar una inica tarea

Que caracteristicas tiene, segun su opinion, un codigo limpio?
que el codigo se encargue unicamente de realizar la tarea que tiene encomendada ya sea metodo o clase, que las variables tengan nombres relacionados con su labor dentro del algoritmo, que el codigo solo tenga los comentarios necesarios, que tenga buena identacion.

Describa la razon del patron de diseño usado
El patron que se implemento fue viper esto fue porque este patron cuenta con 5 capas y cada una de ellas viene perfectamente detallado que hacer y no deja dudas sobre su funcionamiento, pues en una capa se implementa la logica de negocio en otra van los cambios a la vista, otra se encarga de la trancision entre vistas, otra contien los modelos etc, a compracion de otros patrones de diseño en donde al contar con menos capas es un poco ambiguo en donde colocar diferentes labores dentro del funcionamiento de un sistema.